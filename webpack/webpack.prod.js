const { merge } = require('webpack-merge');

const { config } = require('./webpack.base');

// plugins
const TerserPlugin = require('terser-webpack-plugin');

const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

/**
 * Prod configuration
 */
module.exports = merge(config, {
  mode: 'production',

  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendor: {
          chunks: 'all',

          name: 'vendor',

          test: /[\\/]node_modules[\\/]/
        },

        styles: {
          test: /\.(css|scss|less)$/,

          enforce: true
        }
      }
    },

    minimize: true,

    minimizer: [
      new CssMinimizerPlugin(),

      new TerserPlugin({ extractComments: false })
    ]
  },

  plugins: [
    /**
     * Clear output
     */
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['./dist']
    })

    /**
     * Make bundle report
     */
    // new BundleAnalyzerPlugin({
    //   analyzerMode: 'static',

    //   reportFilename: '../report/report.html'
    // })
  ]
});
