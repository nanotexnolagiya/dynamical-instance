const path = require("path");

// plugins
const HtmlWebpackPlugin = require("html-webpack-plugin");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

const CircularDependencyPlugin = require("circular-dependency-plugin");

const SimpleProgressPlugin = require("webpack-simple-progress-plugin");

const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");

const production = process.env.NODE_ENV === "production";

/**
 * Loader shortcut
 */
const use = (loader, options = {}) => ({
  loader,
  options,
});

/**
 * Code entry
 */
const entry = ["src/index.tsx"];

/**
 * Output file configuration
 */
const output = {
  publicPath: "/",

  filename: "js/[name].js",

  chunkFilename: "js/[name].chunk.js",

  path: path.resolve(__dirname, "../dist"),

  devtoolModuleFilenameTemplate: "[absolute-resource-path]",

  devtoolFallbackModuleFilenameTemplate: "[absolute-resource-path]?[hash]",
};

/**
 * Options for imports resolving
 */
const resolve = {
  modules: ["node_modules"],

  plugins: [new TsconfigPathsPlugin()],

  extensions: [".wasm", ".ts", ".tsx", ".mjs", ".cjs", ".js", ".json"],

  alias: {
    "@img": "/src/public/img",
    "Admin/core.scss": "/src/modules/Admin/styles/core/index.scss",
    "Customer/core.scss": "/src/modules/Admin/styles/core/index.scss",
    "Supplier/core.scss": "/src/modules/Admin/styles/core/index.scss",
  },
};

/**
 * Rules config
 */
const rules = [
  {
    test: /\.(ts|tsx)$/,

    use: "ts-loader",

    exclude: /node_modules/,
  },
  {
    test: /\.(woff|woff2|otf|eot|ico|ttf)(\?[a-z0-9=.]+)?$/,

    use: use("file-loader", {
      name: "fonts/[name].[ext]",
    }),
  },
  {
    test: /\.(svg|jpg|jpeg|png|gif)$/,

    use: [
      use("file-loader", { name: "img/[name].[ext]" }),

      "image-webpack-loader",
    ],
  },
  {
    test: /(\.css|\.scss)/,

    exclude: [/src\/modules\/.*\/styles\/global/, /node_modules/],

    use: [
      MiniCssExtractPlugin.loader,

      use("css-loader", {
        modules: {
          localIdentName: "[local]__[hash:base64:5]",
          exportLocalsConvention: "camelCaseOnly",
        },
      }),

      "postcss-loader",

      "sass-loader",
    ],
  },

  {
    test: /(\.css|\.scss)/,

    include: [/src\/modules\/.*\/styles\/global/, /node_modules/],

    use: [
      MiniCssExtractPlugin.loader,

      "css-loader",

      "postcss-loader",

      "sass-loader",
    ],
  },
];

/**
 * Plugins list
 */
const plugins = [
  /**
   * Output html
   */
  new HtmlWebpackPlugin({
    inject: true,

    filename: "index.html",

    template: "./src/public/index.html",
  }),

  /**
   * Pretty output
   */
  new FriendlyErrorsWebpackPlugin(),

  /**
   * Progress bar for builds
   */
  new SimpleProgressPlugin({
    progressOptions: {
      clear: true,
    },
  }),

  /**
   * Warn about circular imports
   */
  new CircularDependencyPlugin({
    exclude: /node_modules/,

    failOnError: true,

    allowAsyncCycles: false,
  }),

  /**
   * Css to sep file
   */
  new MiniCssExtractPlugin()
];

/**
 * Base configuration
 */
const config = {
  entry,

  output,

  resolve,

  plugins,

  module: {
    rules,
  },
};

module.exports = {
  config,
  production,
};
