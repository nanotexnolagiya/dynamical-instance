type DashboardProps = {};

/**
 * <Dashboard /> props
 */
const useDashboardProps = (_: DashboardProps) => {};

export { DashboardProps, useDashboardProps };
