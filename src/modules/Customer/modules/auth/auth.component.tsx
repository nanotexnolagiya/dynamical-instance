import React, { Fragment } from "react";
import styles from "./auth.scss";
import { hoc } from "@Customer/packages/utils";
import { useAuthProps } from "./auth.props";
import { Form, Input, Checkbox, Button } from "antd";

/**
 * <Auth />
 */
const Auth = hoc(useAuthProps, ({ onFinish, onFinishFailed, initialValues }) => (
  <Fragment>
    <div className={styles.auth}>
      <Form
        name="basic"
        initialValues={initialValues}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
        className={styles.form}
      >
        <Form.Item
          label="E-mail"
          name="email"
          rules={[{ required: true, message: "Please input your email!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="rememberMe"
          valuePropName="checked"
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  </Fragment>
));

export { Auth };
