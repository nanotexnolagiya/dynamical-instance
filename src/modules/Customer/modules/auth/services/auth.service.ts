import { AuthResult, LoginValues } from '@Customer/auth/models';
import { Response } from '@Customer/models';
import { HttpService } from '@Customer/services';

class AuthService {
  public constructor(private http: HttpService) {}
  
  public signIn = (data: LoginValues): Promise<Response<AuthResult>> => {
    // It's mock data
    return Promise.resolve({
      data: {
        accessToken: ''
      },
      message: 'Successfully'
    })
    // Auth request example
    // return this.http.request<Response<AuthResult>>({
    //   url: '/v1/auth/web/login',
    //   method: 'POST',
    //   data
    // });
  }
}

export { AuthService };
