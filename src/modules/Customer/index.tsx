import React from 'react';
import { createBrowserHistory } from 'history';
import { createStore } from './store';
import { App } from './app.component';
import { Theme, ThemeProvider } from '@Customer/packages/theme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import './styles/global/index.scss';

const theme: Theme = {
  color: {
    primary: '#154C95',
    secondary: '#FDDF98',
    text: '#0E1B44',
    warning: '#FF8E53',
    error: '#D51F1F',
    info: '#FF8E53',
    success: '#FF8E53'
  },

  space: 8
};

const history = createBrowserHistory({
  getUserConfirmation: (_, callback) => {
    callback(false);
  }
});

const store = createStore(history);

const Main = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>
  </ThemeProvider>
);

export default Main