type Response<T = null> = {
  data: T;
  message?: string;
};

export {
  Response
}