type Theme = {
  color: {
    primary: string;
    secondary: string;
    teritary?: string;
    quaternary?: string;
    fifth?: string;

    text?: string;
    error?: string;
    warning?: string;
    info?: string;
    success?: string;
  };

  space: number;
};

export { Theme };
