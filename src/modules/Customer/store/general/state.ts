import { Account } from "@Customer/models";


class GeneralState {
  public ready: boolean = false;
  
  public account: Account
}

export { GeneralState };
