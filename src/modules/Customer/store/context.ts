import { AuthService } from '@Customer/auth/services';
import { API_URL } from '@config'
import {
  AccountService,
  HttpService,
} from '@Customer/services';
import { AxiosRequestConfig } from 'axios';
import { History } from 'history';
import { Store } from 'redux';
import { Storage, StorageKey } from './storage';

/**
 * Get context
 */
const getContext = (history: History, store: Store) => {
  const storage = new Storage();

  const getConfig = (): Partial<AxiosRequestConfig> => {
    const token = storage.get(StorageKey.Token);

    return {
      baseURL: API_URL,
      headers: {
        ...(token ? { authorization: `Bearer ${token}` } : {})
      }
    };
  };

  const onUnauthorized = () => {
    // store.dispatch(logout());

    return true;
  };

  const http = new HttpService(getConfig, {
    401: onUnauthorized
  });

  const api = {
    auth: new AuthService(http),
    account: new AccountService(http)
  };

  return {
    api,
    store,
    storage,
    history
  };
};

/**
 * Saga context
 */
type StoreContext = ReturnType<typeof getContext>;

export { StoreContext, getContext };
