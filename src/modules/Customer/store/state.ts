import { AuthState } from '@Customer/auth/store';
import { GeneralState } from './general/state';

/**
 * App state
 */
type State = {
  auth: AuthState;
  general: GeneralState;
};

export { State };
