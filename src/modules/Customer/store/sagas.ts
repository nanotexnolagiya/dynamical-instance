import { AuthSaga } from '@Customer/auth/store';
import { GeneralSaga } from './general';
import { RouterSaga } from './router';

/**
 * App sagas
 */
const sagas = [
  new GeneralSaga(),
  new AuthSaga(),
  new RouterSaga(),
];

export { sagas };
