import { auth } from '@Customer/auth/store';
import { general } from './general';

const app = {
  auth,
  general,
};

export { app };
