const capitalize = (source: string) =>
  source ? source.charAt(0).toUpperCase() + source.substring(1) : source;

const shortener = (source: string, maxLength: number = 35) =>
  source.length > maxLength ? source.slice(0, maxLength) + '...' : source;

export { capitalize, shortener };
