/**
 * Storage keys to define what we have
 */
enum StorageKey {
  Token = 'accessToken',
}

/**
 * Storage
 */
class Storage {
  /**
   * Set value by key
   */
  public set<T>(name: StorageKey, value: T) {
    return localStorage.setItem(name, value?.toString());
  }

  /**
   * Get value by name
   */
  public get(name: StorageKey) {
    return localStorage.getItem(name);
  }

  /**
   * Remove by key
   */
  public remove(name: StorageKey) {
    localStorage.removeItem(name);
  }
}

export { Storage, StorageKey };
