import { Account } from "@Admin/models";


class GeneralState {
  public ready: boolean = false;
  
  public account: Account
}

export { GeneralState };
