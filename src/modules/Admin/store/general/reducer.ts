import { reducer } from 'redux-chill';
import {
  boot,
  getAccount,
  logout
} from './actions';
import { GeneralState } from './state';

/**
 * General state
 */
const general = reducer(new GeneralState())
  .on([boot.success, boot.failure], state => {
    state.ready = true;
  })
  .on(
    getAccount.success,
    (state, account) => {
      state.account = account;
    }
  )
  .on(logout, state => {
    state.account = null;
  })

export {
  general
};
