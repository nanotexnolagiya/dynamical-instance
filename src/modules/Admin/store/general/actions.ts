import { AuthResult } from '@Admin/auth/models';
import { Account } from '@Admin/models';
import { make } from 'redux-chill';

const boot = make('[general] boot').stage('success').stage('failure');

const authorize = make('[general] authorize')
  .stage((payload: AuthResult) => payload)
  .stage('success');

const getAccount = make('[general] get account').stage(
  'success',
  (payload: Account) => payload
);

const logout = make('[general] logout')
  .stage((url: string = '/') => url)
  .stage('success');

export {
  boot,
  authorize,
  getAccount,
  logout
};
