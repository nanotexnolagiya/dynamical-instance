import { auth } from '@Admin/auth/store';
import { general } from './general';

const app = {
  auth,
  general,
};

export { app };
