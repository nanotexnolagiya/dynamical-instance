import { useEffect, useRef } from 'react';

const useMounted = (callback = (mounted: boolean) => {}) => {
  let isMounted = useRef(false);

  useEffect(() => {
    isMounted.current = true;
    callback(isMounted.current);

    return () => {
      isMounted.current = false;
    };
  }, []);

  return () => isMounted.current;
};

export { useMounted };
