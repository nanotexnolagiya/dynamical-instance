import { Account, Response } from '@Admin/models';
import { HttpService } from './http.service';

class AccountService {
  public constructor(private http: HttpService) {}

  public getCurrentAccount = () => {
    return Promise.resolve({
      data: {
        id: 1324,
        firstName: 'Tashpulatov',
        lastName: 'Islom',
        email: 'nanotexnolagiya@gmail.com'
      },
      message: 'Successfully'
    })
    // return this.http.request<Response<Account>>({
    //   url: '/v1/accounts/current'
    // });
  }
}

export { AccountService };
