import { LoginValues } from '@Admin/auth/models';
import { } from '@Admin/models';
import { make } from 'redux-chill';

const signIn = make('[auth] sign in')
  .stage((payload: LoginValues) => payload)
  .stage('success')
  .stage('failure', (message: string) => message);

export {
  signIn
};
