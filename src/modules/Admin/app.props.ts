import { State } from '@Admin/store';
import { boot } from '@Admin/store/general';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const useAppProps = () => {
  const dispatch = useDispatch();
  const { ready, account } = useSelector(
    (state: State) => state.general
  );

  useEffect(() => {
    dispatch(boot());
  }, []);

  return {
    ready,
    account
  };
};

export { useAppProps };
