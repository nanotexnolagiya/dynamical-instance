import { Auth } from '@Supplier/auth';
import { MainLayout } from '@Supplier/components/main-layout';
import { Dashboard } from '@Supplier/dashboard';
import { hoc } from '@Supplier/packages/utils';
import * as React from 'react';
import { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useAppProps } from './app.props';

/**
 * <App />
 */
const App = hoc(
  useAppProps,
  ({
    ready,
    account
  }) => {
    if (!ready) return null;

    if (account) {
      return (
        <Fragment>
          <Switch>
            <Route path='/' render={() => (
              <MainLayout>
                <Switch>
                  <Route path='' component={Dashboard} />
                </Switch>
              </MainLayout>
            )} />
          </Switch>
        </Fragment>
      )
    }

    return (
      <Fragment>
        <Switch>
          <Route path='/' component={Auth} />
        </Switch>
      </Fragment>
    );
  }
);

export { App };
