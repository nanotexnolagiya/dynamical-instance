import { make } from 'redux-chill';
import { Location } from 'history';

/**
 * Navigate
 */
const navigate = make('[router] navigate').stage(
  (path: string, scroll: boolean = true) => ({ path, scroll })
);

/**
 * Transition
 */
const setLocation = make('[router] set location').stage(
  (location: Location) => location
);

const getLocation = make('[router] get location');

export { navigate, setLocation, getLocation };
