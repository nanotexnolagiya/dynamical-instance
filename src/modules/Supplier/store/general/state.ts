import { Account } from "@Supplier/models";


class GeneralState {
  public ready: boolean = false;
  
  public account: Account
}

export { GeneralState };
