import { } from '@Supplier/models';
import { Called } from '@Supplier/packages/utils';
import { StoreContext } from '@Supplier/store/context';
import { navigate } from '@Supplier/store/router';
import { StorageKey } from '@Supplier/store/storage';
import { Payload, Saga } from 'redux-chill';
import { call, put } from 'redux-saga/effects';
import {
  boot,
  authorize,
  getAccount,
  logout
} from './actions';

class GeneralSaga {
  private *getUserData(context: StoreContext) {
    yield call(this.getAccount.bind(this, null, context));
  }

  @Saga(boot)
  public *boot(_, context: StoreContext) {
    const { storage } = context;

    try {
      if (storage.get(StorageKey.Token)) {
        yield call(this.getUserData.bind(this), context);
      }
    } finally {
      yield put(boot.success());
    }
  }

  @Saga(authorize)
  public *authorize(
    { accessToken }: Payload<typeof authorize>,
    context: StoreContext
  ) {
    yield call(context.storage.set, StorageKey.Token, accessToken);
    yield call(this.getUserData.bind(this), context);

    yield put(authorize.success());
  }

  @Saga(getAccount)
  public *getAccount(_, { api, storage }: StoreContext) {
    try {
      const response: Called<typeof api.account.getCurrentAccount> = yield call(
        api.account.getCurrentAccount
      );

      yield put(
        getAccount.success(response.data)
      );
    } finally {
    }
  }



  @Saga(logout)
  public *logout(url: Payload<typeof logout>, { storage }: StoreContext) {
    yield call(storage.remove, StorageKey.Token);

    yield put(navigate(url));
  }
}

export { GeneralSaga };
