import { auth } from '@Supplier/auth/store';
import { general } from './general';

const app = {
  auth,
  general,
};

export { app };
