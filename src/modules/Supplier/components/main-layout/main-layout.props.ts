import { logout } from "@Supplier/store/general";
import { useDispatch } from "react-redux";

type MainLayoutProps = {};

type SidebarMenuItem = {
  title: string;
  link?: string;
}

/**
 * <MainLayout /> props
 */
const sidebarMenuList: SidebarMenuItem[] = [
  { title: 'Menu 1', link: '' },
  { title: 'Menu 2', link: '' },
  { title: 'Menu 3', link: '' },
  { title: 'Menu 4', link: '' },
]

const useMainLayoutProps = (_: MainLayoutProps) => {
  const dispatch = useDispatch()

  const onBreakpoint = broken => {
    console.log(broken);
  }

  const onCollapse = (collapsed, type) => {
    console.log(collapsed, type);
  }

  const onLogout = () => {
    dispatch(logout())
  }

  return {
    sidebarMenuList,
    onBreakpoint,
    onCollapse,
    onLogout
  }
};

export { MainLayoutProps, useMainLayoutProps };
