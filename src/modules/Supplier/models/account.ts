type Account = {
  id?: number;

  firstName?: string;

  lastName?: string;

  email?: string;

};

export {
  Account
}