import { useState, useLayoutEffect, useRef } from 'react';

/**
 * Get sizes
 */
const get = () => ({
  width: window.innerWidth,
  height: window.innerHeight,
  body: {
    width: document.body.offsetWidth
  }
});

/**
 * Use window sizes
 */
const useWindowSize = (period = 1000, deps = []) => {
  let [state, set] = useState(get());
  let sizeRef = useRef(state);
  let interval = useRef<any>();

  useLayoutEffect(() => {
    interval.current = setInterval(() => {
      const current = get();
      const notEqualBody = current.body.width != sizeRef.current.body.width;

      if (
        current.width != sizeRef.current.width ||
        current.height != sizeRef.current.height ||
        (notEqualBody && current.body.width != sizeRef.current.width)
      ) {
        set(current);
      }

      sizeRef.current = current;
    }, 300);

    return () => {
      clearInterval(interval.current);
    };
  }, []);

  return state;
};

export { useWindowSize };
