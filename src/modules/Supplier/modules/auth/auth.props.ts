import { useDispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { object, string, boolean } from 'yup';
import { LoginValues } from './models';
import { signIn } from './store';

type AuthProps = RouteComponentProps;

const initialValues: LoginValues = {
  email: '',
  password: '',
  rememberMe: false
};

const validationSchema = object({
  email: string().required().trim().nullable().email('Invalid').label('Email'),

  password: string().required().trim().nullable().label('Password'),

  rememberMe: boolean().nullable()
});

const useAuthProps = (_: AuthProps) => {
  const dispatch = useDispatch();

  const onFinish = (values: LoginValues) => {
    dispatch(signIn(values))
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return { onFinish, onFinishFailed, initialValues };
};

export { useAuthProps };
