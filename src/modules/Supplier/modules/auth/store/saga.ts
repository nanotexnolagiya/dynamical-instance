import {
} from '@Supplier/auth/models';
import { Called, putSync } from '@Supplier/packages/utils';
import { StoreContext } from '@Supplier/store/context';
import { authorize } from '@Supplier/store/general';
import { navigate } from '@Supplier/store/router';
import { Payload, Saga } from 'redux-chill';
import { call, put } from 'redux-saga/effects';
import {
  signIn
} from './actions';

class AuthSaga {
  @Saga(signIn)
  public *signIn(payload: Payload<typeof signIn>, { api }: StoreContext) {
    try {
      const response: Called<typeof api.auth.signIn> = yield call(
        api.auth.signIn,
        payload
      );

      yield putSync(authorize(response.data), authorize.success);

      yield put(navigate('/'));
    } catch (error) {
      yield put(signIn.failure('sign in error'));
    }
  }
}

export { AuthSaga };
