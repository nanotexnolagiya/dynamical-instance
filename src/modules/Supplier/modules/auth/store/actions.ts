import { LoginValues } from '@Supplier/auth/models';
import { } from '@Supplier/models';
import { make } from 'redux-chill';

const signIn = make('[auth] sign in')
  .stage((payload: LoginValues) => payload)
  .stage('success')
  .stage('failure', (message: string) => message);

export {
  signIn
};
