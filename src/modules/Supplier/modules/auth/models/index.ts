
type LoginValues = {
  email: string;
  password: string;
  rememberMe: boolean;
};

type AuthResult = {
  accessToken: string;
};

export {
  LoginValues,
  AuthResult
};
