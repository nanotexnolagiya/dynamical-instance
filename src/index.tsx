import React, { Suspense, lazy } from "react";
import { render } from "react-dom";

const Admin = lazy(() => import("./modules/Admin"));
const Customer = lazy(() => import("./modules/Customer"));
const Supplier = lazy(() => import("./modules/Supplier"));

const container = document.getElementById("app");

const hosts = window.location.host.split(".");

const otherMains = {
  customer: <Customer />,
  supplier: <Supplier />,
  admin: <Admin />,
};

render(
  <Suspense fallback={<div>Instance Loading...</div>}>
    {otherMains[hosts[0]] || <ul>
      <li><a href="http://admin.localhost:3000"> admin.localhost:3000 </a></li>
      <li><a href="http://supplier.localhost:3000"> supplier.localhost:3000 </a></li>
      <li><a href="http://customer.localhost:3000"> customer.localhost:3000 </a></li>
    </ul>}
  </Suspense>,
  container
);
